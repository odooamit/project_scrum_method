# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013 E-MIPS (http://www.e-mips.com.ar) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class project_scrum_user_story(osv.osv):
    _name = 'project.scrum.user.story'
    _description = 'Scrum User Story'
    _order = 'sequence'

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Summary', required=True, size=64),
        'sequence': fields.integer('#', select=True, help="Gives the sequence order and a unique identification."),
        'description': fields.text('Description'),
        # TODO: Agregar campos de time remaining y time spent pero calculado por medio de las tasks
        'importance': fields.selection([('nice','Nice to Have'),('must','Must Have'),('great', 'Great'),('good', 'Good'),('normal', 'Normal')], 'Importance'),
        'user_id': fields.many2one('res.users', 'Owner', track_visibility='onchange'),

        # TODO: Ver si hace falta agregar un campo para asignar gente (Assigments de distinto tipo). O directamente, poder elegir gente y asignarles
        # un rol en particular, como puede ser, desarrollador, soporte, QA, etc.
        #'assignment_ids'': fields.many2many('res.users', 'project_user_rel', 'project_id', 'uid', 'Project Members',
            #help="Project's members are users who can have an access to the tasks related to this project.", states={'close':[('readonly',True)], 'cancelled':[('readonly',True)]}),

        'points': fields.integer('Effort Points', help="These points represent a measure of the effort required to complete this User Story. Then it will be calculated in Releases and Sprints and compared to Sprint velocity"),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'feature_id': fields.many2one('project.feature', 'Feature'),
        'sprint_id': fields.many2one('project.scrum.sprint', 'Sprint'),
        'release_id': fields.many2one('project.scrum.release', 'Release'),
        'issue_ids': fields.one2many('project.issue', 'story_id', "Bugs"),
        'task_ids': fields.one2many('project.task', 'story_id', "Task Activities"),
        'categ_ids': fields.many2many('project.category', string='Tags'),
        'date_start': fields.datetime('Planned Start',select=True),
        'date_end': fields.datetime('Planned End',select=True),
        'state': fields.selection([('draft','New'),('open','Open'),('in_progress','In Progress'),('cancelled', 'Cancelled'),('done','Done')], 'Status', required=True),
    }

    _defaults = {
        'state': 'draft', 
    }

    def write(self, cr, uid, ids, fields, context=None):

        # Si se quita del Release, la pasamos a estado New
        if 'release_id' in fields and fields['release_id'] == False:
            fields['state'] = 'draft'

        return super(project_scrum_user_story, self).write(cr, uid, ids, fields, context)

project_scrum_user_story()


class project_scrum_release(osv.osv):
    _name = 'project.scrum.release'
    _description = 'Scrum Release'

    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Name', required=True, size=64),
        'description': fields.html('Description'),
        # TODO: Agregar campos de points completed y points todo calculado por medio de las stories
        'categ_ids': fields.many2many('project.category', string='Tags'),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'feature_ids': fields.one2many('project.feature', 'release_id', 'Feature'),
        'story_ids': fields.one2many('project.scrum.user.story', 'release_id', 'User Stories'),
        'sprint_ids': fields.one2many('project.scrum.sprint', 'release_id', 'Sprint'),

        'issue_ids': fields.one2many('project.issue', 'release_id', "Bugs"),
        #'task_ids': fields.one2many('project.task', 'story_id', "Task Activities"),

        #'date_start': fields.datetime('Planned Start',select=True),
        #'date_end': fields.datetime('Planned End',select=True),
        #'state': fields.selection([('draft','New'),('open','Open'),('in_progress','In Progress'),('cancelled', 'Cancelled'),('done','Done')], 'Status', required=True),
    }

    _defaults = {
    }

project_scrum_release()


class project_scrum_sprint(osv.osv):
    _name = 'project.scrum.sprint'
    _description = 'Scrum Sprint'

    def _points_assigned(self, cr, uid, ids, field_name, arg, context=None):
        result = {}

        for sp in self.browse(cr, uid, ids, context=context):
            points_assigned = 0.0
            for us in sp.story_ids:
                points_assigned += us.points

            for issue in sp.issue_ids:
                points_assigned += issue.points

            diff = sp.velocity - points_assigned
            result[sp.id] = {'points_assigned': points_assigned, 'points_diff': diff}
        return result


    _columns = {
        'id': fields.integer('ID', readonly=True),
        'name': fields.char('Name', required=True, size=64),
        'description': fields.text('Description'),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'story_ids': fields.one2many('project.scrum.user.story', 'sprint_id', 'User Stories'),
        'issue_ids': fields.one2many('project.issue', 'sprint_id', "Bugs"),
        'release_id': fields.many2one('project.scrum.release', 'Release'),
        'categ_ids': fields.many2many('project.category', string='Tags'),
        'date_start': fields.datetime('Planned Start',select=True),
        'date_end': fields.datetime('Planned End',select=True),
        'points_assigned': fields.function(_points_assigned, string='Points Assigned', type='integer', multi='points'),
        'points_diff': fields.function(_points_assigned, string='Points Difference', type='integer', multi='points'),
        'velocity': fields.integer('Velocity', required=True),

        # TODO: Hacer este campo que se calcule segun las User Story
        #'assigned_effort': fields.function('Assigned Effort'),
    }

    _defaults = {
    }

project_scrum_sprint()


class project_task(osv.osv):
    _name = "project.task"
    _inherit = "project.task"

    _columns = {
        'story_id': fields.many2one('project.scrum.user.story', 'User Story'),
    }

project_task()


class project_issue(osv.osv):
    _inherit = 'project.issue'
    _description = 'project issue'
    _columns = {
        'story_id': fields.many2one('project.scrum.user.story', 'User Story'),
        'release_id': fields.many2one('project.scrum.release', 'Release'),
        'sprint_id': fields.many2one('project.scrum.sprint', 'Sprint'),
        'points': fields.integer('Effort Points', help="These points represent a measure of the effort required to complete this Bug. Then it will be calculated in Releases and Sprints"),
    }

project_issue()

class project(osv.osv):
    _inherit = "project.project"

    _columns = {
        'story_backlog_ids': fields.one2many('project.scrum.user.story', 'project_id', 'Backlog', domain=[('state','=','draft')]),
    }

project_issue()


# TODO: Crear Test Cases
#class project_test_case(osv.osv):
