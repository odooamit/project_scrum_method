# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (c) 2013 E-MIPS (http://www.e-mips.com.ar) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from tools.translate import _

class project_create_sprint(osv.osv_memory):
    _name = 'project.create.sprint'
    _description = 'Project Scrum Create Sprint'

    _columns = {
        'name': fields.char('Sprint Name', size=64, required=True, help="New title of the sprint to be created"),
        'project_id': fields.many2one('project.project', 'Project', required=True),
        'release_id': fields.many2one('project.scrum.release', 'Release', required=True),
        'velocity': fields.integer('Velocity'),
        'story_ids': fields.many2many('project.scrum.user.story', 'project_user_story_sprint', 'wizard_id', 'story_id'),
        'issue_ids': fields.many2many('project.issue', 'project_issue_sprint', 'wizard_id', 'issue_id'),
        }

#    def view_init(self, cr, uid, fields_list, context=None):
#        user_story_obj = self.pool.get('project.scrum.user.story')
#
#        if context is None:
#            context = {}
#        project_id = False
#        if context.get('active_model', '') == 'project.scrum.user.story':
#            for story in user_story_obj.browse(cr, uid, context['active_ids'], context=context):
#                if not project_id:
#                    project_id = story.project_id.id
#                else:
#                    if project_id != story.project_id.id:
#                        raise osv.except_osv(_('Error!'), _('You cannot create sprint with User Stories from different Projects.'))
#            pass

    def default_get(self, cr, uid, fields, context=None):
        """
        This function gets default values
        """
        res = super(project_create_sprint, self).default_get(cr, uid, fields, context=context)
        if context is None:
            context = {}

        record_id = context and context.get('active_id', False) or False
        if not record_id:
            return res

        release_obj = self.pool.get('project.scrum.release')
        release_id = record_id
        release_read = release_obj.read(cr, uid, release_id, ['project_id'], context=context)
        project_id = release_read['project_id'][0]

        if 'project_id' in fields:
            res['project_id'] = project_id if project_id else False

        if 'release_id' in fields:
            res['release_id'] = release_id

        return res

    def onchange_velocity(self, cr, uid, ids, project_id, release_id, velocity, context=None):
        res = {}

        story_obj = self.pool.get('project.scrum.user.story')

        if not project_id and not release_id:
            return res

        story_ids = story_obj.search(cr, uid, [('project_id','=',project_id), ('release_id','=',release_id), ('sprint_id','=',False)], context=context)
        
        sprint_points = 0
        stories = []
        for story in story_obj.read(cr, uid, story_ids, ['points'], context=context):
            if sprint_points+story['points'] <= velocity:
                stories.append(story['id'])
                sprint_points += story['points']
            else:
                break


        res['value'] = {'story_ids': stories}
        return res

    def onchange_project_id(self, cr, uid, ids, project_id, release_id, context=None):
        res = {}

        if not project_id:
            return res

        domain_story = [('project_id','=',project_id), ('sprint_id','=',False)]
        if release_id:
            domain_story.append(('release_id','=',release_id))

        res['domain'] = {'story_ids': domain_story, 'issue_ids': domain_story, 'release_id': [('project_id','=',project_id)]}

        return res

    def onchange_stories(self, cr, uid, ids, story_ids, issue_ids, context=None):
        res = {}
        story_obj = self.pool.get('project.scrum.user.story')
        print 'Stories: ', story_ids
        print 'Issues: ', issue_ids

        stories = story_ids[0][2]
        issues = issue_ids[0][2]

        for story in story_obj.read(cr, uid, stories, ['issue_ids'], context=context):
            if story['issue_ids']:
                issues += [x for x in story['issue_ids'] if x not in issues]

        res['value'] = {'issue_ids': issues}
        return res

    def create_sprint(self, cr, uid, ids, context=None):

        sprint_obj = self.pool.get('project.scrum.sprint')
        issue_obj = self.pool.get('project.issue')
        story_obj = self.pool.get('project.scrum.user.story')

        data = self.browse(cr, uid, ids[0], context)

        sprint_vals = {
            'name': data.name,
            'project_id': data.project_id.id,
            'release_id': data.release_id.id,
            'velocity': data.velocity,
        }

        sprint_id = sprint_obj.create(cr, uid, sprint_vals, context=context)

        sprint_stories = []
        sprint_issues = []

        for story in data.story_ids:
            sprint_stories.append(story.id)

        for issue in data.issue_ids:
            sprint_issues.append(issue.id)

        story_obj.write(cr, uid, sprint_stories, {'sprint_id': sprint_id}, context=context)
        issue_obj.write(cr, uid, sprint_issues, {'sprint_id': sprint_id}, context=context)

project_create_sprint()
